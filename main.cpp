#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>
#define size 50 // can be changed
class Compute {

private:
	std::fstream dataFILE;
	long long int sum, product, min, array_size;
	int *array_nums;
	double average;
	std::string filename;
public:
	Compute();
	void readFile();
	void computations();
	void display();
};

int main() {

	std::cout.setf(std::ios::fixed);
	std::cout.setf(std::ios::showpoint);
	std::cout.precision(2);

	Compute *com = new Compute();

	// read from file
	com->readFile();

	// computations

	com->computations();

	// display all results
	com->display();

	delete com;
	std::cout << "\n\n";
	return 0;
}

Compute::Compute() {

	sum = 0;
	product = 1;
	min = 0;
	average = 0.00;
	array_size = 0;
	array_nums = new int[size];
	filename = "input.txt";
}

void Compute::readFile() {

	std::cout << "Enter filename: ";
	std::cin >> filename;
	
	dataFILE.open(filename, std::ios::in);

	if (!dataFILE) {

		std::cout << "File failed to opened" << std::endl;
		exit(-1);
	}
    
	std::cout << "File successfully opened" << std::endl;

	dataFILE >> array_size;

	int num, i = 0;

	while (!dataFILE.eof() && i < array_size) {

		dataFILE >> num;
		array_nums[i] = num;
		i++;
	}

	dataFILE.close();
}

void Compute::computations() {

	// sum

	for (int i = 0; i < array_size; i++) {

		sum += array_nums[i];
	}

	// product

	for (int i = 0; i < array_size; i++) {

		product *= array_nums[i];
	}

	// average

	average = (sum / (double)array_size);

	// minimum
	min = array_nums[0];

	for (int i = 0; i < array_size; i++) {

		if (array_nums[i] < min) {

		    min = array_nums[i];
		}
	}
}

void Compute::display() {

	std::cout << "\n\n" << "Array" << std::endl;
	std::cout <<  "***************************************" << std::endl;

	std::cout << "\n[ ";
	for (int i = 0; i < array_size; i++) {

		std::cout << array_nums[i] << " ";
	}
	std::cout << "]\n";

	std::cout << "\n" << "Results" << std::endl;
	std::cout << "***************************************" << std::endl;

	std::cout << "Sum is: " << sum << std::endl;

	std::cout << "Product: " << product << std::endl;

	std::cout << "Average: " << average << std::endl;

	std::cout << "Smallest: " << min << std::endl;
}